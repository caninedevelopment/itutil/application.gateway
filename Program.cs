﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Monosoft.Web.Gateway.Hubs;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using Monosoft.Web.Gateway.Controllers;
using Gateway;
using System.Collections.Generic;
using ITUtil.Common.RabbitMQ.Message;
using ITUtil.Common.RabbitMQ.Event;
using ITUtil.Common.RabbitMQ.Request;
using ITUtil.Common.Config;
using ITUtil.Common.RabbitMQ;
using Monosoft.Gateway.Web;

namespace Monosoft.Web.Gateway
{
    public static class Program
    {
        public static ServerConfiguration config = null;
        public static void Main(string[] args)
        {
            config = GlobalRessources.getConfig().GetSetting<ServerConfiguration>();
            List<MessageQueueConfiguration> configurations = new List<MessageQueueConfiguration>();
            configurations.Add(new EventConfiguration(
                "gateway", 
                new List<string>() { "#" }, 
                GatewayHub.HandleMessage 
                ));

            configurations.Add(new EventConfiguration(
                "gateway_invalidate_user",
                new List<string>() { "token.invalidate.user" },
                MemoryCache.HandleInvalidateUser
                ));
            configurations.Add(new EventConfiguration(
                "gateway_invalidate_token",
                new List<string>() { "token.invalidate.token" },
                MemoryCache.HandleInvalidateToken
                ));


            using (var server = new RequestServer(configurations))
            {
                //server.connection.RecoverySucceeded += Connection_RecoverySucceeded;
                server.connection.ConnectionShutdown += Connection_ConnectionShutdown;
                //server.connection.ConnectionRecoveryError += Connection_ConnectionRecoveryError;
                server.connection.ConnectionBlocked += Connection_ConnectionBlocked;
                server.connection.CallbackException += Connection_CallbackException;
                CreateWebHostBuilder(args).Build().Run();
            }
        }

        private static void Connection_CallbackException(object sender, CallbackExceptionEventArgs e)
        {
            var details = Newtonsoft.Json.JsonConvert.SerializeObject(e.Detail);
            var exception = ExceptionHelper.GetExceptionAsReportText(e.Exception);
            RPCController.LogTrace($".EventClient_Connection_CallbackException.log : " + details + "\r\n" + exception);
        }

        private static void Connection_ConnectionBlocked(object sender, ConnectionBlockedEventArgs e)
        {
            RPCController.LogTrace($".EventClient_Connection_ConnectionBlocked.log : " + e.Reason);
        }

        private static void Connection_ConnectionRecoveryError(object sender, ConnectionRecoveryErrorEventArgs e)
        {
            var exception = ExceptionHelper.GetExceptionAsReportText(e.Exception);
            RPCController.LogTrace($".EventClient_Connection_ConnectionRecoveryError.log : " + exception);
        }

        private static void Connection_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            RPCController.LogTrace($".EventClient_Connection_ConnectionShutdown.log : " + e.ReplyText);
        }

        private static void Connection_RecoverySucceeded(object sender, EventArgs e)
        {
            RPCController.LogTrace($".EventClient_Connection_RecoverySucceeded.log : true");
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) => WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
    }
}
