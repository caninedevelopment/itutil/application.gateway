﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Monosoft.Web.Gateway.Hubs;
using System;

namespace Gateway
{
    public class Startup
    {

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var origins = Monosoft.Web.Gateway.Program.config.CORS.Split(',');
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    //.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithOrigins(origins) //"https://bizondev.monosoft.dk", "http://bizondev.monosoft.dk", "https://localhost:44302", "https://localhost:8100",  "http://localhost:8100", "https://localhost:8200", "http://localhost:8200", "https://localhost:4100", "http://localhost:4100", "https://localhost:4200", "http://localhost:4200", "https://auth.monosoft.dk", "https://itutil.monosoft.dk")
                    .SetIsOriginAllowed(_ => true)
                    .AllowCredentials());
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None;
                options.Secure = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
            });

            //services.AddMvc(
            //       o => o.EnableEndpointRouting = false
            //    );//.SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddControllers(controller =>
            {
            });
            services.Configure<IISServerOptions>(option => { option.AllowSynchronousIO = true; });
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.AddSignalR();

            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    //BearerFormat = "Token",//GUID?
                    //Scheme = "bearer",
                    Description = "TokenId from login",
                    Name = "TokenId",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    //Reference = new OpenApiReference()
                    //{
                    //    Id = "Bearer",
                    //    Type = ReferenceType.SecurityScheme
                    //}
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                   {
                     new OpenApiSecurityScheme
                     {
                         Scheme="Bearer",
                        Name = "TokenId",
                        In= ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Reference = new OpenApiReference
                        {
                          Type = ReferenceType.SecurityScheme,
                          Id = "Bearer"
                        }
                     },
                      new string[] { }
                    }
                  });
            });

        }

        public static IHubContext<GatewayHub> signalRHub;

        public void Configure(IApplicationBuilder app/*, IHostingEnvironment env*/)
        {

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/api/", "IT-Util API");
                c.DisplayRequestDuration();
                c.DocumentTitle = "IT-util API";
                c.HeadContent = "<h1>Hey danny... kunne du lave noget standard html til mig med IT-util og monosoft logo...</h1>";
                c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
            });


            app.Use(async (context, next) =>
            {
                await next();
                signalRHub = context.RequestServices.GetRequiredService<IHubContext<GatewayHub>>();
            });

            app.UseCors("CorsPolicy");
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSignalR(routes =>
            {
                routes.MapHub<GatewayHub>("/GatewayHub");
            });
        }
    }
}
