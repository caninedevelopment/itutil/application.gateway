﻿using Gateway;
using Microsoft.AspNetCore.SignalR;
using System;
using Monosoft.Gateway.Web;
using System.Collections.Generic;
using Monosoft.Web.Gateway.Controllers;
using System.Threading.Tasks;
using ITUtil.Common.Base;
using ITUtil.Common.RabbitMQ.Request;
using ITUtil.Common.RabbitMQ.Message;
using ITUtil.Common.RabbitMQ;

namespace Monosoft.Web.Gateway.Hubs
{
    public class GatewayHub : Hub
    {
        public async void WriteMessage(
            string route,
            string messageid,
            string json,
            Guid userContextToken
            )
        {
            string ip = this.Context.GetHttpContext().Connection.RemoteIpAddress.ToString();
            string clientid = this.Context.ConnectionId;
            var tokendata = await MemoryCache.FindToken(userContextToken);
            RequestClient.FAFInstance.FAF(route, new RabbitMqHeader() { clientId=clientid, isDirectLink=false/*?*/, messageId=messageid, messageIssueDate=DateTime.Now, tokenInfo=tokendata, Ip = ip }, json, Gateway.Program.config.InternalServername);
        }

        public async Task<string> RPC(
            string route,
            string messageid,
            string json,
            Guid userContextToken
            )
        {
            string ip = this.Context.GetHttpContext().Connection.RemoteIpAddress.ToString();
            string clientid = this.Context.ConnectionId;
            var tokendata = await MemoryCache.FindToken(userContextToken);
            var result = RPCController.DoRPCCall(route, new RabbitMqHeader() { clientId = clientid, isDirectLink = false/*?*/, messageId = messageid, messageIssueDate = DateTime.Now, tokenInfo = tokendata, Ip = ip }, json);
            return Newtonsoft.Json.JsonConvert.SerializeObject(result);
        }

    
        public static async Task<string> GetDirectLinkData(Guid key)
        {
            cleanupDirectLinkData();
            if (directlinkDictionary.ContainsKey(key))
            {
                var result = directlinkDictionary[key];
                return result.Json;
            }
            else return await Task<string>.Run(()=> { return Newtonsoft.Json.JsonConvert.SerializeObject(LocalizedString.Create("EN", "Data not found"));  } );
        }

        private static void AddDirectLink(Guid key, string json)
        {
           cleanupDirectLinkData();
            lock (padlock)
            {
                directlinkDictionary.Add(key, new DirectLinkInfo() { Json = json, Timestamp = DateTime.Now });
            }
        }

        private static readonly object padlock = new object();
        private static void cleanupDirectLinkData()
        {
            lock (padlock)
            {
                DateTime timeToLive = DateTime.Now.AddSeconds(60);
                List<Guid> ItemsToBeRemoved = new List<Guid>();

                foreach (var entry in directlinkDictionary)
                {
                    if (entry.Value.Timestamp > timeToLive)
                    {
                        ItemsToBeRemoved.Add(entry.Key);
                    }
                }
                foreach (var remove in ItemsToBeRemoved)
                {
                    directlinkDictionary.Remove(remove);
                }
            }
        }

        private static System.Collections.Generic.Dictionary<Guid, DirectLinkInfo> directlinkDictionary = new System.Collections.Generic.Dictionary<Guid, DirectLinkInfo>();
        public static void HandleMessage(string[] topicparts, ReturnMessage wrapper)
        {
            try
            {
                var context = Startup.signalRHub;
                int maxLength = 32000;
                string json = "";
                string responseToClientId = null;
                string responseToMessageId = null;

                if (topicparts[0] == "diagnostics")
                {
                    if (topicparts[1] == "trace")
                    {
                        json = System.Text.Encoding.UTF8.GetString(wrapper.data);
                        responseToClientId = wrapper.responseToClientId;
                        responseToMessageId = wrapper.responseToMessageId;
                    }
                }
                else
                {
                    GatewayReturnMessage returnobj = new GatewayReturnMessage(wrapper);
                    returnobj.dataType = "json";
                    responseToClientId = wrapper.responseToClientId;
                    responseToMessageId = wrapper.responseToMessageId;
                    returnobj.route = string.Join(".", topicparts);

                    json = Newtonsoft.Json.JsonConvert.SerializeObject(returnobj);
                }

                if (json.Length > maxLength)
                {
                    var id = Guid.NewGuid();
                    AddDirectLink(id, json);

                    GatewayReturnMessage re = new GatewayReturnMessage()
                    {
                        message = LocalizedString.OK,
                        responseToMessageId = responseToMessageId,
                        success = true,
                        data = "https://auth.monosoft.dk/api/directlink/" + id.ToString(),
                        dataType = "url"
                    };
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(re);
                }

                if (string.IsNullOrEmpty(responseToClientId) == false)
                {
                    context.Clients.Client(responseToClientId).SendAsync(
                        string.Join('.', topicparts),
                        json
                    ).Wait();
                }
                else
                {
                    context.Clients.All.SendAsync(
                        string.Join('.', topicparts),
                        json
                    ).Wait();
                }
            } catch (Exception ex)
            {
                RPCController.LogTrace($"SignalR.Exception in Gatewayhub handler: {string.Join('.', topicparts)} exception: {ExceptionHelper.GetExceptionAsReportText(ex)}");
            }
        }
    }



}
