﻿using System;

namespace Monosoft.Web.Gateway.Hubs
{
    public class DirectLinkInfo
    {
        public DateTime Timestamp { get; set; }
        public string Json { get; set; }
    }



}
