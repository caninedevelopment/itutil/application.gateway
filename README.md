## Introduction

IT-Util is a framework for microservice development. The Gateway proviedes access
to the solution through HTTPS.


## Features

IT-Util abstracts away the details of distributed systems. Here are the main features.

Service Discovery

Load Balancing

Message Encoding

Request/Response

Async Messaging

Pluggable Interfaces
