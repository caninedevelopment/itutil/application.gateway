﻿// <copyright file="MemoryCache.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Gateway.Web
{
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.Common.RabbitMQ.Message;
    using ITUtil.Common.RabbitMQ.Request;
    using Monosoft.Web.Gateway;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Contains static lists for in-memory representation of user and token data in order to reduce load on the database
    /// </summary>
    public class MemoryCache
    {
        private static MemoryCache instance = null;

        public static MemoryCache Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MemoryCache();
                }
                return instance;
            }
        }

        private readonly List<TokenInfo> TokenCache = new List<TokenInfo>();

        /// <summary>
        /// Clear a specific token from the cache
        /// </summary>
        /// <param name="tokenId">the tokenid to remove from the cache</param>
        internal void InvalidateToken(TokenInfo token)
        {//TODO: mangler listninger
            foreach (var oldtoken in MemoryCache.Instance.TokenCache.Where(p => p.tokenId == token.tokenId).ToList())
            {
                oldtoken.validUntil = token.validUntil;
            }
        }

        /// <summary>
        /// Clear a specific token from the cache
        /// </summary>
        /// <param name="tokenId">the tokenid to remove from the cache</param>
        internal void InvalidateUsers(InvalidateUserData data)
        {
            foreach (var user in data.userIds)
            {
                foreach (var oldtoken in MemoryCache.Instance.TokenCache.Where(p => p.userId == user).ToList())
                {
                    oldtoken.validUntil = data.validUntil;
                }
            }
        }
        public static void HandleInvalidateUser(string[] topicparts, ReturnMessage wrapper)
        {
            var json = System.Text.Encoding.UTF8.GetString(wrapper.data);
            InvalidateUserData user = Newtonsoft.Json.JsonConvert.DeserializeObject<InvalidateUserData>(json);
            Instance.InvalidateUsers(user);
        }
        public static void HandleInvalidateToken(string[] topicparts, ReturnMessage wrapper)
        {
            var json = System.Text.Encoding.UTF8.GetString(wrapper.data);
            TokenInfo token = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenInfo>(json);
            Instance.InvalidateToken(token);
        }


        /// <summary>
        /// Add token data to the memeory cache
        /// </summary>
        /// <param name="token">The token to add</param>
        internal static void AddToken(TokenInfo token)
        {
            MemoryCache.Instance.TokenCache.Add(token);
        }

        /// <summary>
        /// Find tokendata in the cache from a token - will automatically get from remote server (RPC) if needed
        /// </summary>
        /// <param name="token">The token to find</param>
        /// <param name="orgContext">The organisation context to find it in</param>
        /// <returns>The found token data</returns>
        public static async Task<TokenInfo> FindToken(Guid tokenid)
        {
            if (tokenid != Guid.Empty)
            {
                var res = MemoryCache.Instance.TokenCache.Where(p => p.tokenId == tokenid).FirstOrDefault();
                if (res == null)
                {
                    using (var client = RequestClient.RPCInstance)
                    {
                        string useVersion = string.IsNullOrEmpty(Program.config.TokenVerifyVersion) ? "v2" : Program.config.TokenVerifyVersion;

                        ReturnMessage result = client.Rpc("token."+ useVersion + ".verify", new RabbitMqHeader()
                        {
                            clientId = "N/A",
                            messageId = "N/A",
                            messageIssueDate = DateTime.Now,
                            isDirectLink = false,
                            tokenInfo = null/*??*/
                        },
                        "{ tokenId: \""+tokenid.ToString()+"\" }",
                        "N/A").Result;

                        if (result.success)
                        {
                            var innerres = System.Text.Encoding.UTF8.GetString(result.data);
                            res = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenInfo>(innerres);
                            if (res != null)
                                MemoryCache.Instance.TokenCache.Add(res);
                        } else
                        {
                            throw new Exception("Unable to verify token: " + Newtonsoft.Json.JsonConvert.SerializeObject( result.message));
                        }
                    }
                }
                return res;
            } else
            {
                throw new Exception("TokenId should not be GUID.Empty");
            }
        }
    }
}
