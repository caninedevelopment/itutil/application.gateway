﻿using System;
using Microsoft.AspNetCore.Mvc;
using Monosoft.Gateway.Web;
using System.Threading.Tasks;
using Monosoft.Gateway.Web.DTO;
using System.Collections.Generic;
using ITUtil.Common.RabbitMQ.Message;
using ITUtil.Common.RabbitMQ.Request;
using ITUtil.Common.RabbitMQ.DTO;

namespace Monosoft.Web.Gateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RPCController : ControllerBase
    {
        [HttpPost]
        [RequestSizeLimit(524288000)]
        public async Task<string> Post([FromBody]apiMessage message)
        {
            string ip = HttpContext.Connection.RemoteIpAddress.ToString();
            string clientid = string.Empty;

            TokenInfo tokendata = new TokenInfo() { claims = new List<TokenInfo.Claim>() , tokenId = message.token, userId = Guid.Empty, validUntil = DateTime.Now };
            if (message.token != Guid.Empty)
            {
                tokendata = await MemoryCache.FindToken(message.token);
            }

            var res = await RPCController.DoRPCCall(message.route, new RabbitMqHeader() { clientId = clientid, Ip = ip, isDirectLink = false, messageId = message.messageId, messageIssueDate = DateTime.Now, tokenInfo = tokendata } , message.json);
            return Newtonsoft.Json.JsonConvert.SerializeObject(res);
        }

        private static readonly object filepadlock = new object();
        public static void LogTrace(string data)
        {
            lock(filepadlock)
            {
                System.IO.File.AppendAllText(DateTime.Today.ToString("yyyy_MM_dd") + ".log", $"{DateTime.Now} {data}");
            }
        }
        public static async Task<GatewayReturnMessage> DoRPCCall(string route, RabbitMqHeader header, string messageAsJson)
        {
            try
            {
                using (var client = RequestClient.RPCInstance)
                {
                    var response = await client.Rpc(route, header, messageAsJson, Gateway.Program.config.InternalServername);

                    if (response == null)
                    {
                        LogTrace($"RPC -ERROR: response IS NULL, on route:{route} clientid:{header.clientId}  messageid:{header.messageId}");
                        return null;
                    }

                    return new GatewayReturnMessage(response);
                }
            }
            catch (Exception ex)
            {
                LogTrace($"Critical error: " + ex.Message);
                return null;
            }
        }
    }
}