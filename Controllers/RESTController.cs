﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Monosoft.Web.Gateway.Hubs;
using Monosoft.Web.Gateway.Controllers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Collections.Generic;
using Microsoft.OpenApi.Models;
using Microsoft.OpenApi.Writers;
using ITUtil.Common.RabbitMQ.DTO;
using ITUtil.Common.RabbitMQ.Message;
using System.Web;

namespace Monosoft.Gateway.Web.Controllers
{
    
    public class RESTController : Controller
    {
        //[Produces("text/html")]
        [Route("api")]
        public string Help()
        {
            var returnMessage = RPCCall(this.HttpContext, "microservicediscovery.v1.get", "", false, null, true).Result;
            var serviceDescriptions = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceDescriptors>(returnMessage.data);
            if (returnMessage.success)
            {
                var document = new OpenApiDocument();
                document.Info = new OpenApiInfo();
                document.Info.Version = "all";
                document.Info.Title = "IT-Util API";
                document.Info.Description = "Gateway for IT-util";
                document.Info.TermsOfService = new Uri("https://itutil.monosoft.com/terms");
                document.Info.Contact = new OpenApiContact
                {
                    Name = "Monosoft ApS",
                    Email = string.Empty,
                    Url = new Uri("https://www.monosoft.dk/"),
                };


                document.Paths = new OpenApiPaths();
                document.Tags = new List<OpenApiTag>();

                foreach (var serviceDescription in serviceDescriptions.Services.OrderBy(p=>p.serviceName).ThenBy(p=>p.version.ToString()))
                {
                    var openapiPath = new OpenApiPathItem();
                    var url = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/{serviceDescription.serviceName}/scarfoldTS";
                    var operationId = "scarfoldTS";
                    document.Paths.Add($"{serviceDescription.serviceName}/" + operationId, openapiPath);
                    OpenApiOperation openapiOperation = new OpenApiOperation
                    {
                        OperationId = operationId,
                        Description = "Scarfold TypeScript for this service" + $"<br/><a href='{url}'>{url}</a>",
                        Responses = new OpenApiResponses { },
                        RequestBody = null
                    };
                    openapiOperation.Responses.Add("200", new OpenApiResponse
                    {
                        Description = "ok",
                        Content = null
                    });
                    openapiOperation.Tags = new List<OpenApiTag>();
                    openapiOperation.Tags.Add(new OpenApiTag() { Name = serviceDescription.serviceName });//Group...
                    openapiPath.AddOperation(OperationType.Get, openapiOperation);

                    foreach (var methodDescription in serviceDescription.operationDescriptions)
                    {
                        openapiPath = new OpenApiPathItem();
                        operationId = $"v{serviceDescription.version.major}/{methodDescription.operation}";
                        url = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/{serviceDescription.serviceName}/v{serviceDescription.version.major}/{methodDescription.operation}";
                        openapiPath.Summary = serviceDescription.serviceName;

                        string claimstxt = "<br/>Required claims: ";
                        if (methodDescription.RequiredClaims != null && methodDescription.RequiredClaims.Any())
                        {
                            claimstxt += string.Join(", ", methodDescription.RequiredClaims.Select(p => $"<b>{p.key}</b>"));
                        }

                        if (methodDescription.Type == ITUtil.Common.Command.OperationType.Event)
                        {
                            document.Paths.Add($"EVENTS({serviceDescription.serviceName}.v{serviceDescription.version.major}): " + methodDescription.operation, openapiPath);
                            openapiOperation =
                                        new OpenApiOperation
                                        {
                                            OperationId = operationId,
                                            Description = methodDescription.description,
                                            Responses = new OpenApiResponses { },
                                        };
                            openapiOperation.Responses.Add("200", new OpenApiResponse
                            {
                                Description = $"Listen to signalR on hub: 'gateway' on '{methodDescription.operation}'",
                                Content = methodDescription.dataOutputExample == null ? null : new Dictionary<string, OpenApiMediaType>
                                {
                                    ["application/json"] = new OpenApiMediaType
                                    {
                                        Schema = methodDescription.dataOutputExample
                                    },
                                },
                            });

                            openapiOperation.Tags = new List<OpenApiTag>();
                            openapiOperation.Tags.Add(new OpenApiTag() { Name = serviceDescription.serviceName });//Group...
                        }
                        else
                        {
                            document.Paths.Add($"{serviceDescription.serviceName}/" + operationId, openapiPath);
                            openapiOperation =
                                        new OpenApiOperation
                                        {
                                            OperationId = operationId,
                                            Description = methodDescription.description + claimstxt + $"<br/><a href='{url}'>{url}</a>",
                                            Responses = new OpenApiResponses { },
                                        };

                            if (methodDescription.Type == ITUtil.Common.Command.OperationType.Get)
                            {
                                openapiOperation.Parameters = new List<OpenApiParameter> {
                                            new OpenApiParameter() { In = ParameterLocation.Query,
                                //TODO:     Query2Json.ConvertTo()
                                             Name="param1_TODO:",
                                               Schema=methodDescription.dataInputExample
                                            }
                                        };
                            }
                            else
                            {
                                openapiOperation.RequestBody = methodDescription.dataInputExample.Properties.Any() ? new OpenApiRequestBody
                                {
                                    Content = new Dictionary<string, OpenApiMediaType>
                                    {
                                        ["application/json"] = new OpenApiMediaType { Schema = methodDescription.dataInputExample },
                                    },
                                }
                                    : null;

                            }


                            if (methodDescription.dataOutputExample != null)
                            {
                                openapiOperation.Responses.Add("200", new OpenApiResponse
                                {
                                    Description = "ok",
                                    Content = methodDescription.dataOutputExample == null ? null : new Dictionary<string, OpenApiMediaType>
                                    {
                                        ["application/json"] = new OpenApiMediaType
                                        {
                                            Schema = methodDescription.dataOutputExample
                                        },
                                    },
                                });
                            }
                            else
                            {
                                openapiOperation.Responses.Add("204", new OpenApiResponse { Description = "no content" });
                            }

                            if (methodDescription.RequiredClaims != null && methodDescription.RequiredClaims.Any())
                            {
                                openapiOperation.Responses.Add("401", new OpenApiResponse { Description = "unauthorized" });
                                openapiOperation.Responses.Add("403", new OpenApiResponse { Description = "forbidden" });
                            }
                            openapiOperation.Responses.Add("408", new OpenApiResponse { Description = "request timeout" });
                            openapiOperation.Responses.Add("500", new OpenApiResponse { Description = "internal server error" });

                            openapiOperation.Security = new List<OpenApiSecurityRequirement>();
                            openapiOperation.Tags = new List<OpenApiTag>();
                            openapiOperation.Tags.Add(new OpenApiTag() { Name = serviceDescription.serviceName });//Group...

                            if (methodDescription.RequiredClaims != null && methodDescription.RequiredClaims.Any())
                            {//TODO: hvorfor virker det ikke?
                                OpenApiSecurityRequirement requirement = new OpenApiSecurityRequirement();
                                requirement.Add(new OpenApiSecurityScheme()
                                {//TODO: TEST
                                 //BearerFormat = "Bearer",
                                 //Scheme = "Bearer",
                                    Description = "TokenId from login",
                                    Name = "TokenId",
                                    In = ParameterLocation.Header,
                                    Type = SecuritySchemeType.ApiKey,
                                }, new List<string>() { });

                                openapiOperation.Security.Add(requirement);
                            }
                        }
                        switch (methodDescription.Type)
                        {
                            case ITUtil.Common.Command.OperationType.Get:
                                openapiPath.AddOperation(OperationType.Get, openapiOperation);
                                break;
                            case ITUtil.Common.Command.OperationType.Update:
                                openapiPath.AddOperation(OperationType.Put, openapiOperation);
                                break;
                            case ITUtil.Common.Command.OperationType.Insert:
                                openapiPath.AddOperation(OperationType.Post, openapiOperation);
                                break;
                            case ITUtil.Common.Command.OperationType.Delete:
                                openapiPath.AddOperation(OperationType.Delete, openapiOperation);
                                break;
                            case ITUtil.Common.Command.OperationType.Event:
                                openapiPath.AddOperation(OperationType.Options, openapiOperation);
                                break;
                            default:
                                openapiPath.AddOperation(OperationType.Patch, openapiOperation);
                                break;
                        }
                    }
                }
                using (var outputString = new StringWriter())
                {
                    var writer = new OpenApiJsonWriter(outputString);
                    document.SerializeAsV3(writer);
                    return outputString.ToString();
                }
            }
            else
            {
                return "";
            }
        }

        public class ServicesInformation
        {
            public string Name { get; set; }
            public ServiceDescriptor Description { get; set; }
        }

        [Route("internal/directlink/{folder}/{file}")]
        public async Task<string> directlink(string folder, string file)
        {
            return await System.IO.File.ReadAllTextAsync(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory() + @"\directlink\" + folder, file));
        }        

        [Route("api/log")]
        public string Log()
        {
            return System.IO.File.ReadAllText(DateTime.Today.ToString("yyyy_MM_dd") + ".log");
        }

        [HttpGet("api/directlink/{id}")]
        public async Task<string> IndexGet(Guid id)
        {
            return await GatewayHub.GetDirectLinkData(id);
        }

        [HttpGet("{service?}/{version?}/{operation?}")]
        public async Task<string> IndexGetData(string service, string version, string operation)
        {
            try
            {
                var route = $"{service}.{version}.{operation}";

                string json = "{}";
                if (string.IsNullOrEmpty(HttpContext.Request.QueryString.Value) == false)
                {
                    json = Query2Json.ConvertTo(HttpContext.Request.QueryString.Value.Substring(1)/*remove ?*/);
                } 
                var res = await RPCCall(HttpContext, route, json, false);
                HttpContext.Response.StatusCode = res.HTTPStatusCode;
                if (res.HTTPStatusCode == 500)
                {
                    return res.data;//benyt .data her, da interne serverfejl har trace i .data feltet
                }
                else
                if (res.HTTPStatusCode < 200 || res.HTTPStatusCode > 299)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(res.message);
                }
                else
                {
                    return res.data; // Newtonsoft.Json.JsonConvert.DeserializeObject(res.data) as string;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpPost("{service?}/{version?}/{operation?}")]
        [HttpPut("{service?}/{version?}/{operation?}")]
        [HttpDelete("{service?}/{version?}/{operation?}")]
        public async Task<string> IndexBodyData(string service, string version, string operation)
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var json = reader.ReadToEnd();
                    var route = $"{service}.{version}.{operation}";
                    var res = await RPCCall(HttpContext, route, json, false );
                    HttpContext.Response.StatusCode = res.HTTPStatusCode;
                    if (res.HTTPStatusCode == 500)
                    {
                        return res.data;//benyt .data her, da interne serverfejl har trace i .data feltet
                    }
                    else
                    if (res.HTTPStatusCode < 200 || res.HTTPStatusCode > 299)
                    {
                        return Newtonsoft.Json.JsonConvert.SerializeObject(res.message);
                    }
                    else
                    {
                        return res.data; // Newtonsoft.Json.JsonConvert.DeserializeObject(res.data) as string;
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet("api/{service?}/{version?}/{operation?}/{diagnostics?}")]
        public async Task<string> IndexGet(string service, string version, string operation, bool diagnostics = false)
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var json = reader.ReadToEnd();

                    var route = $"{service}.{version}.{operation}";
                    return Newtonsoft.Json.JsonConvert.SerializeObject(await RPCCall(HttpContext, route, json, diagnostics));
                }
            }
            catch (Exception ex)
            {
                //RPCController.LogTrace($".RPCCall : " + ex.Message);
                return ex.Message;
            }
        }

        [HttpPut("api/{service?}/{version?}/{operation?}/{diagnostics?}")]
        [HttpPost("api/{service?}/{version?}/{operation?}/{diagnostics?}")]
        [HttpDelete("api/{service?}/{version?}/{operation?}/{diagnostics?}")]
        public async Task<string> IndexPost(string service, string version, string operation, bool diagnostics = false)
        {
            using (var reader = new StreamReader(Request.Body))
            {
                var json = reader.ReadToEnd();

                var route = $"{service}.{version}.{operation}";
                return Newtonsoft.Json.JsonConvert.SerializeObject(await RPCCall(HttpContext, route, json, diagnostics));
            }
        }

        private static ServiceDescriptors services = null;
        public static async Task<GatewayReturnMessage> RPCCall(HttpContext context, string route, string requestJson, bool includeDiagnostics, Guid? token = null, bool skip404check = false)
        {
            string messageId = "";
            var routeparts = route.Split('.');

            if (routeparts[0] == "microservicediscovery")
                skip404check = true;

            if (context.Request.Headers.ContainsKey("MessageId"))
            {
                messageId = context.Request.Headers["MessageId"];
            }
            if (routeparts.Length >= 2 && routeparts[1] == "scarfoldTS")
            {
                skip404check = true;
            }


            int statuscode = StatusCodes.Status200OK;
            OperationDescription method = null;
            if (skip404check == false)
            {
                if (services == null)
                {
                    var returnMessage = RPCCall(context, "microservicediscovery.v1.get", "", false, token, true).Result;
                    services = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceDescriptors>(returnMessage.data);
                }

                method = (from ser in services.Services
                                   from op in ser.operationDescriptions
                                   where route == $"{ser.serviceName}.v{ser.version.major}.{op.operation}"
                                   select op
                           ).FirstOrDefault();

                if (method == null)
                {//REFRESH
                    var returnMessage = RPCCall(context, "microservicediscovery.v1.get", "", false, token, true).Result;
                    services = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceDescriptors>(returnMessage.data);
                    method = (from ser in services.Services
                                   from op in ser.operationDescriptions
                                   where route == $"{ser.serviceName}.v{ser.version.major}.{op.operation}"
                                   select op

                               ).FirstOrDefault();
                } 

                if (method != null)
                {
                    
                    if (method.dataOutputExample != null) //method.Type == OperationDescription.OperationType.Get)
                    {
                        statuscode = StatusCodes.Status200OK;
                    } else
                    {
                        statuscode = StatusCodes.Status204NoContent;
                    }
                }
            }

            if (skip404check || method != null)
            {
                route = route.Trim('.');
                string ip = context.Connection.RemoteIpAddress.ToString();

                Guid tokenid = token.HasValue ? token.Value : Guid.Empty;

                if (context.Request.Headers.ContainsKey("TokenId"))
                {
                    tokenid = Guid.Parse(context.Request.Headers["TokenId"]);
                }
                if (context.Request.Headers.ContainsKey("tokenid"))
                {
                    tokenid = Guid.Parse(context.Request.Headers["tokenid"]);
                }
                if (context.Request.Cookies.Keys.Contains("TokenId"))
                {
                    tokenid = Guid.Parse(context.Request.Cookies["TokenId"]);
                }
                if (context.Request.Cookies.Keys.Contains("tokenid"))
                {
                    tokenid = Guid.Parse(context.Request.Cookies["tokenid"]);
                }

                Guid organisationId = Guid.Empty;
                if (context.Request.Headers.ContainsKey("OrganisationId"))
                {
                    organisationId = Guid.Parse(context.Request.Headers["OrganisationId"]);
                }
                string clientid = string.Empty;


                TokenInfo tokendata = new TokenInfo() { claims = new List<TokenInfo.Claim>(), tokenId = tokenid, userId = Guid.Empty, validUntil = DateTime.Now };
                try
                {
                    if (tokenid != Guid.Empty)
                    {
                        tokendata = await MemoryCache.FindToken(tokenid);
                    }

                }
                catch
                {
                    return new GatewayReturnMessage()
                    {
                        success = false,
                        HTTPStatusCode = StatusCodes.Status401Unauthorized,
                        message = new ITUtil.Common.Base.LocalizedString[] { new ITUtil.Common.Base.LocalizedString("en", "Unauthorized") },
                        diagnostics = "",
                        route = route,
                        responseToMessageId = messageId,
                        data = "{}",
                        dataType = "json"
                    };
                }


                var res = await RPCController.DoRPCCall(route, new RabbitMqHeader()
                {
                    clientId = clientid,
                    tokenInfo = tokendata,
                    messageIssueDate = DateTime.Now,
                    Ip = ip,
                    isDirectLink = false,
                    includeDiagnostics = includeDiagnostics,
                    messageId = messageId
                }, requestJson);

                if (res.success == false && res.message.Length > 0 && res.message[0].text == "Missing credentials")
                {
                    statuscode = StatusCodes.Status403Forbidden;
                } else
                if (res.success == false && res.message.Length > 0 && res.message[0].text.StartsWith("Request timed out"))
                {
                    statuscode = StatusCodes.Status408RequestTimeout;
                } else
                if (res.success == false && res.message.Length > 0 && res.message[0].text != "Error")
                {
                    int errorcode = -1;
                    if (int.TryParse(res.data, out errorcode))
                    {
                        statuscode = errorcode;
                    }
                } else if (res.success == false)
                {
                    statuscode = StatusCodes.Status500InternalServerError;
                }

                res.HTTPStatusCode = statuscode;

                return res;
            } else
            {
                return new GatewayReturnMessage()
                {
                    success = false,
                    HTTPStatusCode = StatusCodes.Status501NotImplemented,
                    message = new ITUtil.Common.Base.LocalizedString[] { new ITUtil.Common.Base.LocalizedString("en", "Not implemented") },
                    route = route,
                    responseToMessageId = messageId,
                    data = "{}",
                    dataType = "json"
                };

            }
        }
    }
}
