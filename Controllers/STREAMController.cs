﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System;
using Microsoft.AspNetCore.Http;
using ITUtil.Common.RabbitMQ.DTO;

namespace Monosoft.Web.Gateway.Controllers
{
    public class StreamController : Controller
    {
        [HttpGet("api/streamcookie/")]
        public string SetStreamCookie()
        {
            Set("TokenId", Request.Headers["TokenId"], 1);
            return "Done";
        }
        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddDays(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddDays(1);

            option.Path = "/";
            option.HttpOnly = false;
            option.IsEssential = true;

            Response.Cookies.Append(key, value, option);
        }


        //public static System.Collections.Generic.Dictionary<string, Stream> Streams = new System.Collections.Generic.Dictionary<string, Stream>();
        public static System.Collections.Generic.Dictionary<string, HttpClient> Clients = new System.Collections.Generic.Dictionary<string, HttpClient>();
        public static System.Collections.Generic.Dictionary<string, Uri> StreamUri = new System.Collections.Generic.Dictionary<string, Uri>();

        [HttpGet("api/stream/{service?}/{version?}/{operation?}/{json}/{token}")]
        public async Task<FileStreamResult> Get(string service, string version, string operation, string json, string token, bool includeDiagnostics = false) //string uri, string username, string password)
        {
            //string key = $"{service}_{version}_{operation}_{json}_{token}";//?? går nok ikke...

            ResponseStreamUri streamuri = null;
            var route = $"{service}.{version}.{operation}";
            var res = await Monosoft.Gateway.Web.Controllers.RESTController.RPCCall(HttpContext, route, json, includeDiagnostics, string.IsNullOrEmpty(token) ? null : (Guid?)Guid.Parse(token));
            if (res.success)
            {
                streamuri = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseStreamUri>(res.data);
            }

            if (streamuri != null)
            {
                Uri uri = new Uri(streamuri.uri);

                if (StreamUri.ContainsKey(uri.Host) == false)
                {
                    HttpClient sClient;
                    if (!string.IsNullOrEmpty(streamuri.username) && !string.IsNullOrEmpty(streamuri.password))
                    {
                        HttpClientHandler sHandler = new HttpClientHandler();
                        sHandler.Credentials = new NetworkCredential(streamuri.username, streamuri.password);
                        sClient = new HttpClient(sHandler);
                    }
                    else
                    {
                        sClient = new HttpClient();
                    }

                    var stream = await sClient.GetStreamAsync(streamuri.uri);//TODO: placer streams i et dictonary baseret på .uri.... og send stream ud til flere (multicast), så mere end 1 kan lytte på samme stream
                    Clients.Add(uri.Host, sClient);
                    StreamUri.Add(uri.Host, new Uri(streamuri.uri));
                    return ReturnStream(streamuri, stream);
                }
                else
                {
                    var stream = await Clients[uri.Host].GetStreamAsync(StreamUri[uri.Host]);
                    return ReturnStream(streamuri, stream);
                }
            }
            else
            {
                return null;
            }
             
        }

        private static FileStreamResult ReturnStream(ResponseStreamUri streamuri, Stream stream)
        {
            if (streamuri.uri.ToLower().Contains("mjpg"))
            {
                return new FileStreamResult(stream, "multipart/x-mixed-replace; boundary=myboundary");
            }
            else
            {
                return new FileStreamResult(stream, "video/mp4");
            }
        }

        [HttpPut("api/stream/{service?}/{version?}/{operation?}")]
        [HttpPost("api/stream/{service?}/{version?}/{operation?}")]
        public async Task<FileStreamResult> Body(string service, string version, string operation) //string uri, string username, string password)
        {
            ResponseStreamUri streamuri = null;
            using (var reader = new StreamReader(Request.Body))
            {
                var json = reader.ReadToEnd();

                var route = $"{service}.{version}.{operation}";
                var res = await Monosoft.Gateway.Web.Controllers.RESTController.RPCCall(HttpContext, route, json,false);
                if (res.success)
                {
                    streamuri = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseStreamUri>(res.data);
                }
            }
            if (streamuri != null)
            {
                HttpClient sClient;
                if (!string.IsNullOrEmpty(streamuri.username) && !string.IsNullOrEmpty(streamuri.password))
                {
                    HttpClientHandler sHandler = new HttpClientHandler();
                    sHandler.Credentials = new NetworkCredential(streamuri.username, streamuri.password);
                    sClient = new HttpClient(sHandler);
                }
                else
                {
                    sClient = new HttpClient();
                }

                var stream = await sClient.GetStreamAsync(streamuri.uri);//TODO: placer streams i et dictonary baseret på .uri.... og send stream ud til flere (multicast), så mere end 1 kan lytte på samme stream
                return new FileStreamResult(stream, new Microsoft.Net.Http.Headers.MediaTypeHeaderValue("multipart/x-mixed-replace; boundary=myboundary"));
            }
            else
            {
                return null;
            }
        }

    }
}