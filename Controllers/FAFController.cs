﻿using System;
using Microsoft.AspNetCore.Mvc;
using Monosoft.Gateway.Web;
using Monosoft.Gateway.Web.DTO;
using System.Collections.Generic;
using ITUtil.Common.RabbitMQ.DTO;
using ITUtil.Common.RabbitMQ.Request;
using ITUtil.Common.RabbitMQ.Message;

namespace Monosoft.Web.Gateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FAFController : ControllerBase
    {
        [HttpPost]
        [RequestSizeLimit(524288000)]
        public async void Post([FromBody]apiMessage message)
        {
            string ip = HttpContext.Connection.RemoteIpAddress.ToString();
            string clientid = string.Empty;
            TokenInfo tokendata = new TokenInfo() { claims = new List<TokenInfo.Claim>(), tokenId = message.token, userId = Guid.Empty, validUntil = DateTime.Now };
            if (message.token != Guid.Empty)
            {
                tokendata = await MemoryCache.FindToken(message.token);
            }
            RequestClient.FAFInstance.FAF(message.route, new RabbitMqHeader() 
            { 
                clientId=clientid, 
                Ip=ip, 
                isDirectLink=false, 
                messageId=message.messageId, 
                messageIssueDate=DateTime.Now, 
                tokenInfo=tokendata 
            }, message.json,  Gateway.Program.config.InternalServername);
        }
    }
}