﻿namespace Gateway
{
    public class ServerConfiguration
    {
        public string InternalServername { get; set; }
        public string CORS { get; set; }
        public string TokenVerifyVersion { get; set; }
    }
}
