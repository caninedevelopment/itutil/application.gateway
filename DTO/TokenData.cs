﻿using ITUtil.Common.Command;
using System;
using System.Collections.Generic;

namespace Monosoft.Gateway.Web.DTO
{
    public class TokenData
    {
        /// <summary>
        /// Gets or sets Tokenid
        /// </summary>
        public Guid tokenId { get; set; }

        /// <summary>
        /// Gets or sets Userid
        /// </summary>
        public Guid userId { get; set; }

        /// <summary>
        /// Gets or sets ValidUntil
        /// </summary>
        public DateTime validUntil { get; set; }

        /// <summary>
        /// Gets or sets Claims
        /// </summary>
        public List<Claim> claims { get; set; }

        /// <summary>
        /// Gets or sets OrganisationClaims
        /// </summary>
        //public ITUtil.Common.DTO.MetaData[] OrganisationClaims { get; set; }

        public bool IsValidToken()
        {
            return this.validUntil > DateTime.Now;
        }

    }

}
