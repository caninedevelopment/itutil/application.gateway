﻿//using Microsoft.OpenApi.Models;
//using System;
//using System.Collections.Generic;
//using System.Reflection;

//namespace Monosoft.Gateway.DTO
//{
//    public class OperationDescription
//    {
//        //public static List<OperationDescription> Convert(List<ITUtil.Common.Utils.Bootstrapper.OperationDescription> instanceDescriptions)
//        //{
//        //    List<OperationDescription> res = new List<OperationDescription>();
//        //    foreach (var instanceDescription in instanceDescriptions)
//        //    {
//        //        res.Add(new OperationDescription(instanceDescription));
//        //    }
//        //    return res;
//        //}
//        //public OperationDescription()
//        //{
//        //}
//        //    public OperationDescription(ITUtil.Common.Utils.Bootstrapper.OperationDescription instanceDescription)
//        //{
//        //    this.operation = instanceDescription.operation;
//        //    this.description = instanceDescription.description;
//        //    this.dataInputExample = ConvertTypeToOpenApi(instanceDescription.dataInputType);
//        //    this.dataOutputExample = ConvertTypeToOpenApi(instanceDescription.dataOutputType);
//        //    this.RequiredClaims = instanceDescription.RequiredClaims;
//        //    if (this.RequiredClaims == null)
//        //    {
//        //        this.RequiredClaims = new ITUtil.Common.DTO.MetaDataDefinitions(new ITUtil.Common.DTO.MetaDataDefinition[] { });
//        //    }
//        //}

//        private OpenApiSchema ConvertTypeToOpenApi(Type datatype)
//        {

//            OpenApiSchema schema = new OpenApiSchema();
//            if (datatype != null)
//            {
//                schema.Type = "object";
//                schema.Properties = new Dictionary<string, OpenApiSchema>();
//                foreach (var property in datatype.GetProperties())
//                {
//                    if (property.CanWrite && property.CanRead)
//                    {
//                        if (IsSimple(property.PropertyType))
//                        {

//                            schema.Properties.Add(
//                                property.Name,
//                                new OpenApiSchema()
//                                {
//                                    Type = this.GetPropertyJSType(property),
//                                    Description = "", //TODO!
//                                    Title = "", //TODO!
//                                    //Example = "", //TODO
//                                    Format = "", //TODO
//                                });
//                        } else
//                        { // TODO: hvordan skal vi håndtere cirkulær referencer?
//                            schema.Properties.Add(
//                                property.Name,
//                                ConvertTypeToOpenApi(property.PropertyType)
//                                );
//                        }
//                    }
//                }

//                return schema;
//            } else
//            {
//                return null;
//            }
//        }

//        private bool IsSimple(Type type)
//        {
//            var typeInfo = type.GetTypeInfo();
//            if (typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>))
//            {
//                // nullable type, check if the nested type is simple.
//                return IsSimple(typeInfo.GetGenericArguments()[0]);
//            }
//            return typeInfo.IsPrimitive
//              || typeInfo.IsEnum
//              || type.Equals(typeof(string))
//              || type.Equals(typeof(decimal));
//        }
//        private string GetPropertyJSType(PropertyInfo pi)
//        {
//            switch (pi.PropertyType.Name.ToLower())
//            {
//                case "string":
//                    return "string";
//                case "int":
//                case "int16":
//                case "int32":
//                case "double":
//                case "decimal":
//                    return "number";
//                case "long":
//                case "int64":
//                    return "bigint";
//                case "bool":
//                case "boolean":
//                    return "boolean";
//                default:
//                    throw new Exception("Unknown datatype " + pi.PropertyType.Name + " for " + pi.Name);
//            }
//        }
//        public string operation { get; set; } //ex. "Insert", "Update", "Delete", "Get" etc.
//        public string description { get; set; } //developer description
//        public OpenApiSchema dataInputExample { get; set; } //json description
//        public OpenApiSchema dataOutputExample { get; set; } //json description
//                                                      //public List<EventDescription> events { get; set; }

//        public ITUtil.Common.DTO.MetaDataDefinitions RequiredClaims { get; set; }
//    }
//}
