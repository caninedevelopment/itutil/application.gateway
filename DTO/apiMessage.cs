﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monosoft.Gateway.Web.DTO
{
    public class apiMessage
    {

        public string route { get; set; }

        public string messageId { get; set; }

        public string json { get; set; }

        public Guid token { get; set; }

//        public Tracing.Level tracing { get; set; }
    }

}
