﻿using ITUtil.Common.Base;
using ITUtil.Common.RabbitMQ.Message;

namespace Monosoft.Gateway.Web
{
    public class GatewayReturnMessage
    {
        public GatewayReturnMessage()
        {
        }
        public GatewayReturnMessage(ReturnMessage data)
        {
            this.diagnostics = data.diagnostics;
            this.responseToMessageId = data.responseToMessageId;
            this.message = data.message;
            this.success = data.success;
            this.data = data.data == null ? "" : System.Text.Encoding.UTF8.GetString(data.data);
            this.dataType = "json";
            this.HTTPStatusCode = 200;
        }

        public string responseToMessageId { get; set; }
        public LocalizedString[] message { get; set; }
        public bool success { get; set; }
        public string diagnostics { get; set; }
        public string route { get; set; }
        public string data { get; set; }
        public string dataType { get; set; }
        public int HTTPStatusCode { get; set; }

    }
}
